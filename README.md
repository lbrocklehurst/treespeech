# TreeSpeech #
### v1.0.0.0 ###
A basic Dialogue Tree framework, written in C#

## Features ##

* Creation and Reading of Dialogue Tree files
* Chaining of dialogue and Response requests
* Easy Dialogue file generation using [TreeSpeechGenerator](https://bitbucket.org/lbrocklehurst/treespeechgenerator)

## How to Use ##

Create a Dialogue file using [TreeSpeechGenerator](https://bitbucket.org/lbrocklehurst/treespeechgenerator) or edit the example *example1.tspeech* file.

Load your Dialogue file:
```
#!c#
Dialogue myDialogue = new Dialogue(@"C:\directory", "example1.tspeech");
```

Return the current speech string with:
```
#!c#
myDialogue.Speak();
```

Get an array of any available responses to the current Entry:
```
#!c#
string[] responses = myDialogue.GetResponses();
```

Then respond to one of these with:
```
#!c#
int response = 1;
myDialogue.Respond(response);
```

Or just continue onto the next part of dialogue:
```
#!c#
myDialogue.Continue();
```