﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TreeSpeech;

namespace TreeSpeechTest
{
    [TestClass]
    public class SaveAndLoadTest
    {
        [TestMethod]
        public void LoadTest()
        {
            Dialogue s = new Dialogue();
            Entry newEntry = new Entry();
            newEntry.ID = 1;
            newEntry.TAG = Globals.Tags.START;
            newEntry.Text = "Hello World";
            newEntry.AddResponse(3);
            newEntry.AddResponse(6);
            Entry secondEntry = new Entry();
            secondEntry.ID = 3;
            secondEntry.Text = "This is a response";
            secondEntry.Pointer = 4;
            Entry lastEntry = new Entry();
            lastEntry.ID = 4;
            lastEntry.Text = "This is the last piece of text";
            s.Tree.Entries.Add(newEntry);
            s.Tree.Entries.Add(secondEntry);
            s.Tree.Entries.Add(lastEntry);
            s.Save(@"C:\temp", "treesave.sav");

            Console.WriteLine(s.Speak());
            Console.WriteLine("<Responding with 1...>");
            Console.WriteLine(s.Respond(1));
            Console.WriteLine("<Continue...>");
            Console.WriteLine(s.Continue());

            //Dialogue dialogue = new Dialogue(@"C:\temp", "treesave.sav");
        }
    }
}
