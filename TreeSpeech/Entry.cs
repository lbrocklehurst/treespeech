﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSpeech
{
    public class Entry
    {
        public int ID, Pointer = -1;
        public string Text = "";
        public int[] Responses;
        public Globals.Tags TAG;

        /// <summary>
        /// Returns bool if the Entry has a pointer assigned to it or not
        /// </summary>
        public bool HasPointer { get { return (Pointer > 0); } }

        /// <summary>
        /// Initialises an Entry class which hold the information for a line of dialogue
        /// </summary>
        public Entry(){}
        public Entry(int id)
        {
            this.ID = id;
        }
        public Entry(int id, string text)
        {
            this.ID = id;
            this.Text = text;
        }

        /// <summary>
        /// Add a link to a response
        /// </summary>
        /// <param name="id">ID number of response</param>
        public void AddResponse(int id)
        {
            List<int> list = (Responses != null && Responses.Length > 0) ? Responses.ToList() : new List<int>();    // If a response exists already, add to array
            list.Add(id);
            Responses = list.ToArray();
        }
    }
}
