﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSpeech
{
    public class Globals
    {
        public enum Tags
        {
            NONE,
            START,
            RESPONSE
        }
        public const string Filetype = ".tspeech";
    }
}
