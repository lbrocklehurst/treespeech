using System.Collections;
using System.Xml.Serialization;
using System.IO;
using System;

public class Serializer
{
	public static bool Serialize<T>(T data, string location, string name)
	{
		string fullPath = Path.Combine(location, name);
		try
		{
			XmlSerializer writer = new XmlSerializer(typeof(T));
			using (FileStream stream = new FileStream(fullPath, FileMode.Create))
			{
				writer.Serialize(stream, data);
			}
            return true;
		}
		catch(Exception e)
		{
			Exception ie = e.InnerException;
			if(ie != null)
				Console.WriteLine("Exception: " + ie.ToString());
			else
                Console.WriteLine("Exception: " + e.ToString());
		}
        return false;
	}

	public static T Deserialize<T>(string location, string name)
	{
		string fullPath = Path.Combine(location, name);
		T serializedData = default(T);
		try
		{
			XmlSerializer writer = new XmlSerializer(typeof(T));
			using (FileStream stream = new FileStream(fullPath, FileMode.Open))
			{
				serializedData = (T)writer.Deserialize(stream);
			}
			return serializedData;
		}
		catch(Exception e)
		{
			Exception ie = e.InnerException;
			if(ie != null)
                Console.WriteLine("Exception: " + ie.ToString());
			else
                Console.WriteLine("Exception: " + e.ToString());
			return serializedData;
		} 
	}
}

