﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSpeech
{
    public class Tree
    {
        public List<Entry> Entries = new List<Entry>();
        public int Current = -1;

        /// <summary>
        /// Get the current active Entry
        /// </summary>
        public Entry CurrentEntry
        {
            get { return GetEntry(Current); }
        }
        /// <summary>
        /// Get Entry of ID
        /// </summary>
        public Entry GetEntry(int id)
        {
            Entry entry = Entries.Find(x => x.ID == id);
            if(entry == null)
                Console.WriteLine(string.Format("[Tree.cs] GetEntry({0}): entry with ID:{0} returned NULL", id));
            return entry;
        }
        /// <summary>
        /// Sets the start entry by searching for the START tag
        /// </summary>
        /// <returns>returns the start entry's ID, or -1 if null</returns>
        private int SetStart()
        {
            Entry startEntry = Entries.Find(x => x.TAG == Globals.Tags.START); // Find Entry with START tag
            if (startEntry == null)
            {
                Console.WriteLine("[Tree.cs] SetStart(): startEntry returned NULL - please set an Entry to tag START");
                return -1;
            }
            Current = startEntry.ID;
            return Current;
        }
        /// <summary>
        /// Returns the Start or Current Entry text
        /// </summary>
        public string Speak()
        {
            if (Current < 0 && SetStart() < 0)
                return "!ERROR! NO DIALOGUE START SPECIFIED";
            return CurrentEntry.Text;
        }
        /// <summary>
        /// Continue's the Dialogue Tree by asking for a Pointer
        /// </summary>
        public string Continue()
        {
            Entry entry = CurrentEntry;
            if(entry == null)
                return "!ERROR! NO ENTRY EXISTS WITH ID:" + Current;
            if(entry.HasPointer)
            {
                Entry newEntry = GetEntry(entry.Pointer);
                if (entry == null)
                    return "!ERROR! NO ENTRY EXISTS WITH ID:" + entry.Pointer;
                Current = entry.Pointer;
                return newEntry.Text;
            }
            return entry.Text;
        }
        /// <summary>
        /// Send a Response to the current Entry
        /// </summary>
        /// <param name="option">The specified option (index of Entry's Responses int[] + 1)</param>
        /// <returns>return the associated response</returns>
        public string Respond(int option)
        {
            // TODO: Clean up error handling
            Entry currentEntry = CurrentEntry;
            if(currentEntry == null)
                return "!ERROR! NO ENTRY EXISTS WITH ID:" + Current;
            if(currentEntry.Responses == null)
            {
                Console.WriteLine(string.Format("[Tree.cs] Respond(): Entry has no responses!", option, currentEntry.Responses.Length));
                return string.Format("!ERROR! ENTRY HAS NO RESPONSES", option);
            }
            int index = option - 1;
            if (currentEntry.Responses.Length <= 0 || option > currentEntry.Responses.Length)
            {
                Console.WriteLine(string.Format("[Tree.cs] Respond(): Invalid Option: {0}, Response Length: {1}", option, currentEntry.Responses.Length));
                return string.Format("!ERROR! INVALID OPTION: {0}", option);
            }
            int id = currentEntry.Responses[index];
            Entry responseEntry = GetEntry(id);
            if(responseEntry == null)
            {
                Console.WriteLine(string.Format("[Tree.cs] Respond(): entry returned NULL - specified response with ID:{0} does not exist", id));
                return "!ERROR! NO RESPONSE EXISTS WITH ID:" + id;
            }
            Current = responseEntry.Pointer;
            return responseEntry.Text;
        }

        /// <summary>
        /// Returns an array of strings representing the repsonses
        /// </summary>
        /// <returns></returns>
        public string[] GetResponses()
        {
            if (CurrentEntry.Responses == null || CurrentEntry.Responses.Length <= 0)
                return null; // No responses, return null
            string[] responses = new string[CurrentEntry.Responses.Length];

            for (int i = 0; i < CurrentEntry.Responses.Length; i++)
                responses[i] = GetEntry(CurrentEntry.Responses[i]).Text;

            return responses;
        }
    }
}
