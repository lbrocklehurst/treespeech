﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TreeSpeech
{
    public class Dialogue
    {
        public Tree Tree;

        /// <summary>
        /// Contains information for Dialogue Tree
        /// </summary>
        public Dialogue()
        {
            this.Tree = new Tree();
        }
        /// <param name="location">Location of dialogue tree, including filename</param>
        public Dialogue(string location)
        {
            Load(location);
        }
        /// <param name="location">Location of dialogue tree</param>
        /// <param name="filename">Filename of dialogue tree</param>
        public Dialogue(string location, string filename)
        {
            Load(location, filename);
        }
        /// <summary>
        /// Start the Dialogue Tree
        /// </summary>
        public string Speak() { return Tree.Speak(); }
        /// <summary>
        /// Continue onto next Entry
        /// </summary>
        public string Continue() { return Tree.Continue(); }
        /// <summary>
        /// Send a Response to the current Entry
        /// </summary>
        /// <param name="option">The specified option (index of Entry's Responses int[] + 1)</param>
        public string Respond(int option) { return Tree.Respond(option); }

        /// <summary>
        /// Returns an array of strings representing the responses
        /// </summary>
        /// <returns></returns>
        public string[] GetResponses()
        {
            return Tree.GetResponses();
        }
        /// <summary>
        /// Check if the current entry has responses
        /// </summary>
        public bool HasResponses
        {
            get { return (Tree.CurrentEntry.Responses != null && Tree.CurrentEntry.Responses.Length > 0); }
        }

        /// <summary>
        /// Load the dialogue tree
        /// </summary>
        /// <param name="location">Location of tree including filename</param>
        private void Load(string location)
        {
            FileInfo locInfo = new FileInfo(location);
            Debug.WriteLine(string.Format("Loading... Path: {0}, Filename: {1}", locInfo.Directory.FullName, locInfo.Name));
            Tree = Serializer.Deserialize<Tree>(locInfo.Directory.FullName, locInfo.Name);
        }
        /// <param name="location">Location of tree</param>
        /// <param name="filename">Filename of tree file</param>
        private void Load(string location, string filename)
        {
            Debug.WriteLine(string.Format("Loading... Path: {0}, Filename: {1}", location, filename));
            this.Tree = Serializer.Deserialize<Tree>(location, filename);
        }
        /// <summary>
        /// Save the dialogue tree
        /// </summary>
        /// <param name="location">Location of tree including filename</param>
        public bool Save(string location)
        {
            FileInfo locInfo = new FileInfo(location);
            Debug.WriteLine(string.Format("Saving... Path: {0}, Filename: {1}", locInfo.Directory.FullName, locInfo.Name));
            return Serializer.Serialize<Tree>(this.Tree, locInfo.Directory.FullName, locInfo.Name);
        }
        /// <param name="location">Location of tree</param>
        /// <param name="filename">Filename of tree file</param>
        public bool Save(string location, string filename)
        {
            Debug.WriteLine(string.Format("Saving... Path: {0}, Filename: {1}", location, filename));
            return Serializer.Serialize<Tree>(this.Tree, location, filename);
        }
    }
}
